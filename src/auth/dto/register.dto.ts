import { IsAlpha, IsAlphanumeric, IsEmail, IsNotEmpty, IsOptional, Length } from "class-validator";

export class RegisterDto {

    @IsAlphanumeric()
    @IsNotEmpty()
    @Length(4,20)
    username: string;

    @IsAlpha()
    @Length(0,60)
    @IsNotEmpty()
    first_name: string;

    @IsOptional()
    @IsAlpha()
    @Length(0,60)
    middle_name: string;

    @IsNotEmpty()
    @IsAlpha()
    @Length(0,60)
    last_name: string;
 
    @IsEmail()
    @Length(0,70)
    email: string;
   
    @Length(8,32)
    password: string

}